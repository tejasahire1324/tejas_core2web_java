class SwitchI2W{
	public static void main(String[] args){
		String friends = "Kanha";
		System.out.println("before switch");
		switch(friends){
			case "Ashish":
					System.out.println("Barclays");
					break;
			case "Kanha":
					System.out.println("Bmc software");
					break;
			case "Rahul":
					System.out.println("infosys");
					break;
			case "Badhe":
					System.out.println("IBM");
					break;
			    default :
					System.out.println("In default state");
					break;
		}
		System.out.println("After switch");
	
	
	}



}
